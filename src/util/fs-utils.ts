import * as fs from "fs";

/**
 * Delete (clean) the path if it exists, or create it recursively
 */
export function cleanPath(dirPath: string): void {
  rmRecursive(dirPath);
  mkdirRecursive(dirPath);
}

export function mkdirRecursive(dirPath: string): void {
  fs.mkdirSync(dirPath, { recursive: true });
}

/**
 * Delete a directory and all its sub-directorires and files recursively
 */
export function rmRecursive(dirPath: string): void {
  if (fs.existsSync(dirPath)) {
    fs.readdirSync(dirPath).forEach((file, index) => {
      const curPath = dirPath + "/" + file;
      if (fs.lstatSync(curPath).isDirectory()) { // recurse
        rmRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(dirPath);
  }
};
