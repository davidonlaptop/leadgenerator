/**
 * Returns true if the value is NOT undefined and NOT null
 */
export function isDefined(o: any): boolean {
  return !isUndefined(o) && !isNull(o);
}

/**
 * Returns true if the value is really undefined
 */
export function isUndefined(o: any): boolean {
  return typeof o === 'undefined';
}

export function isObject(o: any): boolean {
  return o !== null && typeof o === 'object';
}

/**
 * Returns true if the value is null
 */
export function isNull(o: any): boolean {
  return o === null;
}
