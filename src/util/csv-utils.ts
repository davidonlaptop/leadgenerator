import fs from "fs";
import Papa, { ParseConfig, ParseError, ParseMeta, ParseResult, Parser } from "papaparse";
import path from "path";
import { mkdirRecursive } from "./fs-utils";
import { camelize } from "./string-utils";
import stream from 'stream';

export type ICsvParseConfig = ParseConfig;
export type ICsvParseError = ParseError;
export type ICsvParseMeta = ParseMeta;

/**
 * Inspired by `ParseResult`
 */
export interface ICsvParseResults<T> {
  /**
   * Parsed data
   */
  rows: T[];
  /**
   * Errors encountered
   */
  errors: ICsvParseError[];
  /**
   * Extra parse info
   */
  meta: ICsvParseMeta;
}

/**
 * Inspired by `ParseResult`
 */
export interface ICsvParseResult<T> {
  /**
   * Parsed data
   */
  row: T;
  /**
   * Errors encountered
   */
  errors: ICsvParseError[];
  /**
   * Extra parse info
   */
  meta: ICsvParseMeta;
}

/**
 * Basically converts `data` to `rows`
 */
function papaToCsvResults<T>(papaResults: ParseResult, file?: File): ICsvParseResults<T> {
  return {
    rows: papaResults.data,
    errors: papaResults.errors,
    meta: papaResults.meta
  };
}

/**
 * Basically converts `data` to a single `row`
 */
function papaToCsvResult<T>(papaResults: ParseResult, file?: File): ICsvParseResult<T> {
  return {
    row: papaResults.data as any,
    errors: papaResults.errors,
    meta: papaResults.meta
  };
}

/**
 * Converts a Papa.Parse callback to an ES6 promise
 * @see https://stackoverflow.com/questions/22519784/how-do-i-convert-an-existing-callback-api-to-promises
 * @see https://zellwk.com/blog/converting-callbacks-to-promises/
 */
async function papaParsePromise<T>(
  input: string | File | NodeJS.ReadableStream,
  config?: ICsvParseConfig
): Promise<ICsvParseResults<T>> {
  return new Promise<ICsvParseResults<T>>((resolve, reject) => {
    const papaConfig: ParseConfig = {
      ...config,
      complete: (papaResults: ParseResult, file?: File) => {
        const results = papaToCsvResults<T>(papaResults, file);
        resolve(results);
      },
      error: reject
    };

    Papa.parse(input, papaConfig);
  });
};


/**
 * Helper to read a CSV file
 * @see https://www.papaparse.com/
 */
export async function readCsvFile<T>(filePath: string, onStep?: (r: ICsvParseResult<T>) => void): Promise<ICsvParseResults<T>> {
  const inputStream = fs.createReadStream(filePath);

  return await readCsvStream(inputStream, onStep);
}

/**
 * Helper to read a CSV file
 * @see https://www.papaparse.com/
 */
export async function readCsvStream<T>(
    inputStream: stream.Readable, 
    onStep?: (r: ICsvParseResult<T>) => void
  ): Promise<ICsvParseResults<T>> {
  // TODO: should make the camelize optional

  const config: ICsvParseConfig = {
    header: true, 
    dynamicTyping: true,  // Converts numeric/boolean data
    transformHeader: (header: string) => {
      // console.info("transformHeader: ", camelize(header));
      return camelize(header);
    }
  }

  if (onStep) {
    config.step = (papaResults: ParseResult, parser: Parser) => {
      // console.info("* STEP: %j", papaResults);
      const result = papaToCsvResult<T>(papaResults);
      onStep(result);
    }
  }

  return await papaParsePromise(inputStream, config);
}


export async function writeCsvFile(filePath: string, data: object[] | any[][]): Promise<void> {  
  // data: Array<Object> | Array<Array<any>> | UnparseObject, config?: UnparseConfig
  const output = Papa.unparse(data, {
    delimiter: "\t"
  });

  const dirName = path.dirname(filePath);
  mkdirRecursive(dirName);
  fs.writeFileSync(filePath, output, "utf-8");
}