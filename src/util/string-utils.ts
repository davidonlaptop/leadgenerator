// import { prettify } from "./hjson-utils";
import { isNull, isObject, isUndefined } from "./object-utils";

/**
 * Returns true if the string is undefined / null, 
 * or contain only whitespaces
 */
export function isBlank(s?: string): boolean {
  return (!s || /^\s*$/.test(s));
}

export enum PaddingDirection {
  LEFT,
  // CENTER,
  RIGHT
}

/**
 * @see https://stackoverflow.com/a/24398129/359793
 */
export function pad(padding: string, str?: string, dir = PaddingDirection.RIGHT) {
  if (typeof str === 'undefined') {
    return padding;
  }

  if (PaddingDirection.RIGHT === dir) {
    return (padding + str).slice(-padding.length);
  } else {
    return (str + padding).substring(0, padding.length);
  }
}

/**
 * Converts any string to camel case
 * @see https://stackoverflow.com/a/2970667/359793
 */
export function camelize(s: string): string {
  return s.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match, index) => {
    if (+match === 0) { return ""; } // or if (/\s+/.test(match)) for white spaces
    return index === 0 ? match.toLowerCase() : match.toUpperCase();
  });
}

/**
 * Converts any string to titlecase format
 * @see https://stackoverflow.com/a/196991/359793
 */
export function toTitleCase(s: string): string {
  return s.replace(/\w\S*/g, (replacer) => {
    return replacer.charAt(0).toUpperCase() + replacer.substr(1).toLowerCase();
  });
}

/**
 * Safely converts anything to a string
 * 
 * @see lodash's _.toString() at https://github.com/lodash/lodash/blob/4.17.15/lodash.js#L12557
 */
export function safeString(value: any): string {
  // TODO: maybe consider using '' + value ??
  
  if (typeof value === 'string') {
    // Exit early for strings to avoid a performance hit in some environments.
    return value;
  } else if (isUndefined(value)) {
    return 'undefined';
  } else if (isNull(value)) {
    // TODO: add support overriding to write 'null'
    return 'null';
    // TODO: add support for symbol
  // } else if (Array.isArray(value)) {
  //   // TODO: see lodash _.toString() for better performance
  //   return prettify(value);
  // } else if (isObject(value)) {
  //   // TODO: see lodash _.toString() for better performance
  //   return prettify(value);
  } else {
    return value;
  }
}

export function safeUppercase(value: any): string {
  const safeValue = '' + value;

  return safeValue.toUpperCase();
}


/**
 * Safely compare 2 strings for sort
 * @see https://stackoverflow.com/a/51169/359793
 */
export function compare(a?: string, b?: string): number {
  // Forcing a & b to be strings to avoid exceptions.
  // localeCompare has been supported since Internet Explorer 6 and Firefox 1
  return ('' + a).localeCompare('' + b);
}

/**
 * Convert accentuated characters to their english equivalent
 * 
 * @see https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
 */
export function englishifyAccents(s: string): string {
  // 1. normalize()ing to NFD Unicode normal form decomposes combined graphemes into the combination of simple ones.
  //    The è of Crème ends up expressed as e + ̀.
  // 
  // 2. Using a regex character class to match the U+0300 → U+036F range, 
  //    it is now trivial to globally get rid of the diacritics, 
  //    which the Unicode standard conveniently groups as the Combining Diacritical Marks Unicode block.

  return s.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

export function hasEnglishLowerCaseLetters(s: string): boolean {
  return (/[a-z]/.test(s));
}

export function hasEnglishUpperCaseLetters(s: string): boolean {
  return (/[A-Z]/.test(s));
}
