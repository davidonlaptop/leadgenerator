import axios from 'axios';
import qs from 'qs';
import { YELP_API_KEY } from './secrets';
import { IYelpBusinessSearch } from './model/IYelpBusinessSearch';
import { IYelpBusinessSearchResults } from './model/IYelpBusinessSearchResults';

const YELP_BUSINESS_SEARCH_URL = "/businesses/search";


function configureAxios() {
  axios.defaults.baseURL = 'https://api.yelp.com/v3';
  axios.defaults.headers.common['Authorization'] = `Bearer ${YELP_API_KEY}`;
  // axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
  axios.defaults.paramsSerializer = (params) => qs.stringify(params, {arrayFormat: 'brackets'});
}


export async function yelpBusinessSearch(params: IYelpBusinessSearch = {}): Promise<IYelpBusinessSearchResults> {
  const response = await axios.get<IYelpBusinessSearchResults>(YELP_BUSINESS_SEARCH_URL, {params});

  return response.data;
}

async function main() {
  // const results = await yelpBusinessSearch({location: "toronto", term: "restaurant"});

  // console.debug("results: %j", results);
}


// Asynchronous main
(async () => {
  try {
    configureAxios();
    // console.info("Starting ...");
    // var text = await main();
  } catch (e) {
    console.error(e);
  }
})();
