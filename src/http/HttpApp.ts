import express, { Application } from 'express';

export interface IHttpAppArgs {
    port: number;
    middleWares: any;
    controllers: any;
}

export class HttpApp {
    public app: Application;
    public port: number;

    constructor(args: IHttpAppArgs) {
        this.app = express();
        this.port = args.port;

        // Disable cache
        this.app.set("etag", false);

        this.middlewares(args.middleWares);
        this.routes(args.controllers);
        this.assets();
    }

    private middlewares(middleWares: { forEach: (arg0: (middleWare: any) => void) => void; }) {
        middleWares.forEach(middleWare => {
            this.app.use(middleWare);
        })
    }

    private routes(controllers: { forEach: (arg0: (controller: any) => void) => void; }) {
        controllers.forEach(controller => {
            this.app.use('/', controller.router);
        })
    }

    private assets() {
        this.app.use(express.static('public'));
    }

    public listen() {
        this.app.listen(this.port, () => {
            console.info(`HTTP Server listening port ${this.port}`);
        })
    }
}
