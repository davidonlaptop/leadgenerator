import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import { HttpApp } from './HttpApp';
import { loggerMiddleware } from './middleware/logger';
import { InternetPlansController } from './controllers/YelpController';
import serveStatic from 'serve-static';


export const CACHE_MAX_AGE_MS = 30 * 60 * 1000;

const isPROD = (process.env.NODE_ENV === 'production');
const PORT = parseInt(process.env.WEB_PORT as any) || (isPROD ? 80 : 5000);

/**
 * https://expressjs.com/en/resources/middleware/cors.html
 * http://expressjs.com/en/resources/middleware/compression.html
 * http://expressjs.com/en/resources/middleware/serve-static.html
 */

// Main
(async () => {
  try {
    // Bootstrap app first

    const corsOptions = {
      origin: '*',
      optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
    };

    console.debug("Configuring HTTP server...");
    const app = new HttpApp({
      port: PORT,
      controllers: [
        new InternetPlansController()
      ],
      middleWares: [ 
        // compression(),
        compression({level: 1}), // FASTEST
        // compression({level: 9}), // SLOWEST (BEST COMPRESSION)
        bodyParser.json(),
        bodyParser.urlencoded({ extended: true }),
        cors(corsOptions),
        loggerMiddleware,
        serveStatic('public', {maxAge: CACHE_MAX_AGE_MS})
      ]
    });

    app.listen();
  } catch (e) {
    console.error(e);
  }
})();