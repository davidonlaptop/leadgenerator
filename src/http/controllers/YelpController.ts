import express from 'express';
import { Request, Response } from 'express';
import { CACHE_MAX_AGE_MS } from '../server';
import { IYelpBusinessSearch } from '../../model/IYelpBusinessSearch';
import { yelpBusinessSearch } from '../../index';
import Papa from 'papaparse';

export class InternetPlansController {
  private router = express.Router();
  
  
  public constructor() {
    // Bind events
    this.healthcheck = this.healthcheck.bind(this);
    // this.parseParams = this.parseParams.bind(this);
    this.searchBusinesses = this.searchBusinesses.bind(this);

    // init routes
    this.router.get('/health', this.healthcheck);
    this.router.get('/search/business', this.searchBusinesses);
  }

  protected async healthcheck(req: Request, res: Response, next: any) {
    // TODO: how to know if it is healthy??
    res.status(200).send("Everything should be fine\n");
  }

  protected async searchBusinesses(req: Request, res: Response, next: any) {
    // this.makeCacheable(res);
    try {
      // console.info("url = %s, originalUrl=%s, params=%j", req.url, req.originalUrl, req.query);
      // console.info("inside search");
      const params = req.query as IYelpBusinessSearch;
      const params2 = {limit: 50, ...params};

      // console.info("searchBusiness()");
      const results = await yelpBusinessSearch(params2);
      // console.info("searchBusiness() after search: %j", results);

      // Convert to CSV
      const header = ["Name", "Phone", "DisplayPhone", "Categories", "Distance", "isClosed", "price", "rating", "review_count", "url"].join(",");

      const rows = results.businesses.map(r => {
        const cells = [];

        cells.push(r.name.replace(',', ';'));
        cells.push(r.phone);
        cells.push(r.display_phone);
        if (Array.isArray(r.categories)) {
          cells.push(" ");
        } else {
          cells.push(undefined);
        }
        cells.push(r.distance);
        cells.push(r.is_closed);
        cells.push(r.price);
        cells.push(r.rating);
        cells.push(r.review_count);
        cells.push(r.url);

        return cells.join(",");
      }).join("\r\n");

      const output = header + "\r\n" + rows;

      // const output = Papa.unparse(rows, {
      //   delimiter: "\t",
      //   header: true,
      //   columns: ["Name", "Phone", "DisplayPhone", "Categories", "Distance", "isClosed", "price", "rating", "review_count", "url"]
      // });
      
      // const results = await this.service.search(args);
      // console.info("copmleted reults")

      if (results.businesses && results.businesses.length > 0) {
        // console.info("received %d results: %s", results.businesses.length, results.businesses.join(","));
        const offset = params2.offset ? params2.offset : 0;
        res.attachment(`yelp-${params2.location}-${params2.term}-${offset}.csv`);
        res.status(200).send(output);
      } else {
        console.info("found 0 results");  
        res.status(404);
        // res.status(404).send("No more results");
        // return;
      }
      // res.attachment('yelp.csv');
      // res.status(200).send(output);
    } catch (error) {
      console.error("error in search: %s", error);
      return next(error);
    }
  }

  private makeCacheable(res: Response) {
    const MAX_AGE_SECONDS = Math.round(CACHE_MAX_AGE_MS/1000);
    
    res.set('Cache-Control', 'public, max-age=' + MAX_AGE_SECONDS);
  }
}