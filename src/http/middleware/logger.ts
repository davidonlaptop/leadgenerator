import { Request, Response } from 'express'

export async function loggerMiddleware(req: Request, res: Response, next: any) {
  const startedAt = Date.now();
  console.info('** > %s %s', req.method, req.url);
  await next();
  console.info('** < %s %s => %d in %d ms.', req.method, req.url, res.statusCode, Date.now() - startedAt);
}
