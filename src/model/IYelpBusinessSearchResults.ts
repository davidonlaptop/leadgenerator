/**
 * @see https://www.yelp.ca/developers/documentation/v3/business_search
 */
export interface IYelpBusinessSearchResults {
  /**
   * int	Total number of business Yelp finds based on the search criteria. 
   * Sometimes, the value may exceed 1000. 
   * In such case, you still can only get up to 1000 businesses using multiple queries and combinations of the "limit" and "offset" parameters.
   */
  total: number;
  /**
   * List of business Yelp finds based on the search criteria.
   */
  businesses: IYelpBusiness[];
}

export interface IYelpCoordinates {
  /**
   * decimal	Latitude of this business.
   */
  latitude: number;
  /**
   * decimal	Longitude of this business.
   */
  longitude: number;
}

export interface IYelpBusinessCategory {
  /**
   * Alias of a category, when searching for business in certain categories, use alias rather than the title.
   */
  alias: string;
  /**
   * Title of a category for display purpose.
   */
  title: string;
}

export interface IYelpBusinessLocation {
  /**
   * Street address of this business.
   */
  address1: string;
  /**
   * Street address of this business, continued.
   */
  address2: string;
  /**
   * Street address of this business, continued.
   */
  address3: string;
  /**
   * City of this business.
   */
  city: string;
  /**
   * ISO 3166-1 alpha-2 country code of this business.
   */
  country: string;
  /**
   * Array of strings that if organized vertically give an address that is in the standard address format for the business's country.
   */
  display_address: string[];
  /**
   * ISO 3166-2 (with a few exceptions) state code of this business.
   */
  state: string;
  /**
   * Zip code of this business.
   */
  zip_code: string;
}

export interface IYelpResultsRegion {
  /**
   * Center position of map area.
   */
  center: {
    /**
     * decimal	Latitude position of map bounds center.
     */
    latitude: number;
    /**
     * decimal	Longitude position of map bounds center.
     */
    longitude: number;
  }
}

export interface IYelpBusiness {
  /**
   * List of category title and alias pairs associated with this business.
   */
  categories: IYelpBusinessCategory[];
  /**
   * Coordinates of this business.
   */
  coordinates: IYelpCoordinates;
  /**
   * Phone number of the business formatted nicely to be displayed to users.
   * The format is the standard phone number format for the business's country.
   */
  display_phone: string;
  /**
   * decimal	Distance in meters from the search   
   * This returns meters regardless of the locale.
   */
  distance: number;
  /**
   * Unique Yelp ID of this business.
   * Example: '4kMBvIEWPxWkWKFN__8SxQ'
   */
  id: string;
  /**
   * Unique Yelp alias of this business.
   * Can contain unicode characters. 
   * Example: 'yelp-san-francisco'. 
   * Also see What's the difference between the Yelp business ID and business alias?
   */
  alias: string;
  /**
   * URL of photo for this business.
   */
  image_url: string;
  /**
   * Whether business has been (permanently) closed
   */
  is_closed: boolean;
  /**
   * Location of this business, including address, city, state, zip code and country.
   */
  location: IYelpBusinessLocation;
  /**
   * Name of this business.
   */
  name: string;
  /**
   * Phone number of the business.
   */
  phone: string;
  /**
   * Price level of the business.
   * Value is one of $, $$, $$$ and $$$$.
   */
  price: string;
  /**
   * decimal	Rating for this business (value ranges from 1, 1.5, ... 4.5, 5).
   */
  rating: number;
  /**
   * int	Number of reviews for this business.
   */
  review_count: number;
  /**
   * URL for business page on Yelp.
   */
  url: string;
  /**
   * List of Yelp transactions that the business is registered for. 
   * Current supported values are pickup, delivery and restaurant_reservation.
   */
  transactions: string[];
  /**
   * Suggested area in a map to display results in.
   */
  region: IYelpResultsRegion;
}